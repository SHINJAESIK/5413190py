import random

def swapFisrtLast(list):
    set=list[0]
    set1=list[-1]
    list[0]=set1
    list[-1]=set
    return list

def shiftElement(list):
    list.insert(0,list[-1])
    list.pop(-1)
    return list

def replaceEven(list):
    for i in range(len(list)):
        if i%2==0:
            list[i]=0
    return list

def replaceNeighbor(list):
    list[1:8].sort()


def removeMiddle(list):
    a=len(list)/2
    a=int(a)
    if len(list)%2==0:
        list.pop(a)
        list.pop(a+1)
    else:
        list.pop(a/2)
    return list

def moveFront(list):
    for i in range(len(list)):
        if list[i]%2==0:
            list.insert(0,list[i])
            list.pop(i+1)
    return list

def secondLargest(list):
    list.sort()
    return list[1]

def isSorted(list):
    list_sort=list.sort()
    if list==list_sort:
        return True
    else:
        return False

def containsAdjacent(list):
    for i in range(len(list)):
        if list[i]==list[i+1]:
            return True

def containsDuplicate(list):
    for i in range(len(list)):
        if list[0]==list[i+1]:
            return True

list=[]
for i in range(10):
    list.append(random.randint(1,100))
print(list)
swapFisrtLast(list)
shiftElement(list)
replaceEven(list)
replaceNeighbor(list)
removeMiddle(list)
moveFront(list)
secondLargest(list)
isSorted(list)
containsAdjacent(list)
containsDuplicate(list)