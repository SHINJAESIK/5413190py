seat=[
    [10,10,10,10,10,10,10,10,10,10],
    [10,10,10,10,10,10,10,10,10,10],
    [10,10,10,10,10,10,10,10,10,10],
    [10,10,20,20,20,20,20,20,10,10],
    [10,10,20,20,20,20,20,20,10,10],
    [10,10,20,20,20,20,20,20,10,10],
    [20,20,30,30,40,40,30,30,20,20],
    [20,30,30,40,50,50,40,30,30,20],
    [30,40,50,50,50,50,50,50,40,30],
]
for i in range(9):
    print("")
    for j in range(10):
        print(seat[i][j],"",end="")
print("")
print("")
y=input("Pick seat or price(S,P) =")

if y.upper()=='S':
    while 1:
        row=int(input("Enter the rows of the seats you want(0~8) = "))
        column=int(input("Enter the columns of the seats you want(0~9) = "))
        if seat[row][column]!=0:
            n=str(input("You can sit there. Do you want to choose that seat?(Y/N) = "))
            if n.upper()=='Y':
                seat[row][column]=0
                print("It's been successfully booked.")
                for i in range(9):
                    print("")
                    for j in range(10):
                        print(seat[i][j],"",end="")
                print("")
                print("")
                q=input("If you want to continue enter please y, If you want to stop, please enter B. = ")
                if q.upper()=='B':
                    break
            else:
                print("Please choose your seat again.")
        else:
            print("That seat is already reserved. Please choose your seat again.")
else:
    while 1:
        z=int(input("Enter the price you want.(10~50) = "))
        if z==10 or z==20 or z==30 or z==40 or z==50:
            print("The seats at that price are = ", end="")
            for i in range(9):
                for j in range(10):
                    if seat[i][j]==z:
                        print("["+str(i)+","+str(j)+"]","",end="")
            print("")
            f=input("Do you want to see a different price of seats?(Y/N) = ")
            if f.upper()=='N':
                break
        else:
            print("Enter the correct price.(10~50)")
