from random import random

TRIES=10000000
hits=0

for i in range(TRIES):
    r=random()
    x=-1+2*r
    r=random()
    y=-1+2*r

    if x*x+y*y<=1:
        hits=hits+1

piEstimate=4.0*hits/TRIES
print("Estimate for pi = ",piEstimate)