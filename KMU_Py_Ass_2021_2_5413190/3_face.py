from ezgraphics import GraphicsWindow

win=GraphicsWindow(640,480)
canvas=win.canvas()

canvas.setOutline('yellow')
canvas.drawOval(40,30,200,200)
canvas.setColor('yellow')
canvas.drawOval(80,100,30,20)
canvas.setColor('black')
canvas.drawOval(90,105,10,10)
canvas.setColor('yellow')
canvas.drawOval(170,100,30,20)
canvas.setColor('black')
canvas.drawOval(180,105,10,10)
canvas.setColor('yellow')
canvas.drawOval(90,175,100,20)
canvas.drawArc(90,60,100,255,30)

win.wait()