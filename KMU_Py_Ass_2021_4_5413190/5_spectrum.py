Type=input("Select Wavelength(W) or Frequency(F) = ")

if Type.upper()=="W":
    Wavelength=float(input("Wavelength : "))
    if Wavelength>10**-1:
        print("Radio Waves")
    elif Wavelength>10**-3 and Wavelength<10**-1:
        print("Microwaves")
    elif Wavelength>7*(10**-7) and Wavelength<10**-3:
        print("Infrared")
    elif Wavelength>4*(10**-7) and Wavelength<7*(10**-7):
        print("Visible light")
    elif Wavelength>10**-8 and Wavelength<4*(10**-7):
        print("Ultraviolet")
    elif Wavelength>10**-11 and Wavelength<10**-8:
        print("X-rays")
    else:
        print("Gamma rays")
else:
    Frequency=float(input("Frequency : "))
    if Frequency<3*(10**9):
        print("Radio Waves")
    elif Frequency>3*(10**9) and Frequency<3*(10**11):
        print("Microwaves")
    elif Frequency>3*(10**11) and Frequency<4*(10**14):
        print("Infrared")
    elif Frequency>4*(10**14) and Frequency<7.5*(10**14):
        print("Visible light")
    elif Frequency>7.5*(10**14) and Frequency<3*(10**16):
        print("Ultraviolet")
    elif Frequency>3*(10**16) and Frequency<3*(10**19):
        print("X-rays")
    else:
        print("Gamma rays")