import math

def sphereVolume(r):
    return (4/3)*math.pi*r**3

def sphereSurface(r):
    return 4*math.pi*r

def cylinderVolume(r,h):
    return math.pi*(r**2)*h

def cylinderSurface(r,h):
    return 2*math.pi*r**2+2*math.pi*r*h

def coneVolume(r,h):
    return (1/3)*math.pi*(r**2)*h

def coneSurface(r,h):
    return math.pi*r*math.sqrt(r**2+h**2)+math.pi*r**2


r=float(input("Input r = "))
h=float(input("Input h = "))

print("sphere volume =",sphereVolume(r),"and sphere surface =",sphereSurface(r))
print("cylinder volume =",coneVolume(r,h),"and cylinder surface =",cylinderVolume(r,h))
print("cone volume =",coneVolume(r,h),"and cone surface =",coneSurface(r,h))
