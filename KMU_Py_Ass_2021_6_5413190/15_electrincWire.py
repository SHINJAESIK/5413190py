import math


def diameter(n):
    return 0.127*92**((36-n)/39)

def copperWireResistance(length,diameter):
    return (4*1.678*10**(-8)*length)/(math.pi*diameter**2)

n=float(input("Input wireGauge = "))
length=float(input("Input length = "))
d=diameter(n)
l=copperWireResistance(length,d)

print("Diameter =",d,"copperWireResistance = ",l)


